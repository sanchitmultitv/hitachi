import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RegistrationDeskRoutingModule } from './registration-desk-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RegistrationDeskRoutingModule
  ]
})
export class RegistrationDeskModule { }
