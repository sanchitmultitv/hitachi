import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BriefcaseRoutingModule } from './briefcase-routing.module';
import { BriefcaseComponent } from './briefcase.component';


@NgModule({
  declarations: [BriefcaseComponent],
  imports: [
    CommonModule,
    BriefcaseRoutingModule
  ]
})
export class BriefcaseModule { }
