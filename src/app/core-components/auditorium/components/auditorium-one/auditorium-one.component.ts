import { Component, OnInit, OnDestroy } from '@angular/core';
import { fadeAnimation } from '../../../../shared/animation/fade.animation';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { FetchDataService } from '../../../../services/fetch-data.service'
import { EventEmitter } from 'events';
import { ChatService } from '../../../../services/chat.service';
declare var $: any;
import * as _ from 'underscore';
import * as Clappr from 'clappr';
@Component({
  selector: 'app-auditorium-one',
  templateUrl: './auditorium-one.component.html',
  styleUrls: ['../auditorimStl/audi.style.scss'],
  animations: [fadeAnimation],

})
export class AuditoriumOneComponent implements OnInit, OnDestroy {
  videoEnd = false;
  // videoPlayer = 'https://d17uqpjc0q0ra5.cloudfront.net/abr/smil:session4.smil/playlist.m3u8';
  interval;
  player:any
  actives: any = [];
  newWidth;
  newHeight;
  vidURL:any
  but=true;
  // stream: any;
  //  videoPlayer = 'https://d17uqpjc0q0ra5.cloudfront.net/abr/smil:session1.smil/playlist.m3u8';
  videoPlayer:any
  constructor(private chatService: ChatService, private _fd: FetchDataService) { }

  ngOnInit(): void {
    this.audiActive();
    this.interval= setInterval(() => {
      this.getHeartbeat(); 
         }, 60000);
  //  this.playVideo();

    // console.log(this.videoPlayer, 'this is 1');
    this.loadData();
    this.chatService.getconnect('toujeo-143');
    this.chatService.getMessages().subscribe((data => {
      //  console.log('data',data);
      if (data == 'groupchat') {
        this.chatGroup();
      }

    }));
    this.butFunc();


    var countDownDate = new Date("Feb 13, 2021 15:37:25").getTime();

    // Update the count down every 1 second
    var x = setInterval(function () {

      // Get today's date and time
      var now = new Date().getTime();

      // Find the distance between now and the count down date
      var distance = countDownDate - now;

      // Time calculations for days, hours, minutes and seconds
      var days = Math.floor(distance / (1000 * 60 * 60 * 24));
      var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
      let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      let seconds = Math.floor((distance % (1000 * 60)) / 1000);

      // Output the result in an element with id="demo"

      if (minutes < 10) {
        this.minutes = '0' + minutes;
      }else{
        this.minutes = minutes
      }
      if (seconds < 10) {
        this.seconds = '0' + seconds;
      }else{
        this.seconds = seconds
      }
      document.getElementById("demo").innerHTML = this.minutes + " : " + this.seconds + " ";

      // If the count down is over, write some text 
      if (distance < 0) {
        clearInterval(x);
        document.getElementById("demo").innerHTML = "EXPIRED";
      }
    }, 1000);
  }
  
  butFunc(){
    let timer: any = new Date().getHours() + ':' + new Date().getMinutes() + ':' + new Date().getSeconds();

    if(timer >= '20:00:00'){
      this.but =false
    }else{
      this.but =true
    }
  }
  audiActive() {
    this._fd.activeAudi('111').subscribe(res => {
       console.log(res, 'resssss');
      // this.actives = res.result;
      this.actives = res.result;
      this.videoPlayer = res.result;
      this.vidURL = res.result[0].stream
      console.log(this.vidURL);
      this.playVideo()
      // console.log(this.videoPlayer+ "YO YO HONEY SINGH")
      // console.log(this.actives[0]);
    })
  }
  playVideo() {
    
    console.log(this.vidURL);
    var playerElement = document.getElementById("player_sec");
    this.player = new Clappr.Player({
      parentId: 'player_sec',
      source: this.vidURL,
      // source: "http://d3tn0h9cityxqm.cloudfront.net/streamline/chunks/701_5ef2e2fc9223a/701_5ef2e2fc9223a_master.m3u8",
      // poster: 'assets/sat.jpg',
      height: '100%',
      maxBufferLength: 30,
      width: '100%',
      autoPlay: true,
      loop: true,
      hideMediaControl: false,
      hideVolumeBar: false,
      hideSeekBar: false,
      persistConfig: false,
      // chromeless: true,
      // mute: true,
      visibilityEnableIcon: false,
      disableErrorScreen: true,
      playback: {
        playInline: true,
        // recycleVideo: Clappr.Browser.isMobile,
        recycleVideo: true
      },
    });
    this.player.attachTo(playerElement);
    // $('#player-wrapper > div > .media-control').css({ 'height': '0'});
    // if (window.innerWidth <= 572) {
    //   this.player.play();
    //   this.player.resize({ width: '100%', height: window.innerWidth / 2 + 30 });
    // } else {
    //   this.player.play();
    //   var aspectRatio = 9 / 16;
    //   // tslint:disable-next-line: prefer-const
    //   this.newWidth = document.getElementById('player-wrapper').parentElement
    //     .offsetWidth;
    //   this.newHeight = 2 * Math.round((this.newWidth * aspectRatio) / 2);
    //   if (this.newWidth < 1000) {
    //   this.player.resize({ width: this.newWidth, height: this.newHeight });
    //   $('.container')
    //     .find('.player-poster')
    //     .css('background-size', 'contain');
    // }
    // }
  }
  videoEnded() {
    this.videoEnd = true;
  }
  getHeartbeat(){
    // alert("ss")
    let data = JSON.parse(localStorage.getItem('virtual'));
    const formData = new FormData();
    formData.append('user_id', data.id );
    formData.append('event_id', '143');
    formData.append('audi', '111');
    this._fd.heartbeat(formData).subscribe(res=>{
      console.log(res);
    })
  }
  playAudioClap() {
    let playaudio: any = document.getElementById('myAudioClap');
    playaudio.play();
  }
  playAudioWhistle() {
    let playaudio: any = document.getElementById('myAudioWhistle');
    playaudio.play();
  }
  openGroupChat() {
    $('.groupchatOne').modal('show');
    this.messageList = [];
    this.loadData();
  }


  textMessage = new FormControl('');
  newMessage: string[] = [];
  msgs: string;
  messageList: any = [];
  roomName = 'myanmar_16';
  serdia_room = localStorage.getItem('serdia_room');
  loadData() {
    this.chatGroup();
    this.chatService.getconnect('toujeo-143');
    let data = JSON.parse(localStorage.getItem('virtual'));
    this.chatService.addUser(data.name, this.roomName);
    localStorage.setItem('username', data.name);
    this.chatService.receiveMessages(this.roomName).subscribe((msgs: any) => {
      if (msgs.roomId === 1) {
        this.messageList.push(msgs);
      }
      console.log('demo', this.messageList);
    });
  }
  chatGroup() {
    this._fd.groupchating().subscribe(res => {
      console.log('groupChat', res);
      this.messageList = res.result;
    });
  }

  closePopup() {
    $('.groupchatOne').modal('hide');
  }
  postMessage(value) {
    let data = JSON.parse(localStorage.getItem('virtual'));
    let yyyy: any = new Date().getFullYear();
    let dd: any = new Date().getDate();
    let mm: any = new Date().getMonth() + 1;
    let time: any = new Date().getHours() + ':' + new Date().getMinutes() + ':' + new Date().getSeconds();
    if (dd < 10) {
      dd = '0' + dd;
    }
    if (mm < 10) {
      mm = '0' + mm;
    }
    let created = yyyy + '-' + mm + '-' + dd + ' ' + time;
  //  this.chatService.sendMessage(value, data.name, this.roomName);
  this._fd.postGroupchat(value,data.name,data.email, this.roomName, created,data.id).subscribe(res=>{
    console.log(res);
  })



    this.textMessage.reset();
    // this.chatGroup();
    //this.newMessage.push(this.msgs);
  }
  ngOnDestroy() {
    clearInterval(this.interval);
    // this.chatService.disconnect();
  }
}

