import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuditoriumThreeComponent } from './auditorium-three.component';


const routes: Routes = [  {path:'', component:AuditoriumThreeComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuditoriumThreeRoutingModule { }
