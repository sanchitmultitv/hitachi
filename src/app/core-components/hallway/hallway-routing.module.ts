import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HallwayComponent } from './hallway.component';

const routes: Routes = [{ path: '', component: HallwayComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HallwayRoutingModule { }
