import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HallwayRoutingModule } from './hallway-routing.module';
import { HallwayComponent } from './hallway.component';


@NgModule({
  declarations: [HallwayComponent],
  imports: [
    CommonModule,
    HallwayRoutingModule
  ]
})
export class HallwayModule { }
