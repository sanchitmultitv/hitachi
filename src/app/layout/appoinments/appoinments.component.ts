import { Component, OnInit } from '@angular/core';
import { FetchDataService } from 'src/app/services/fetch-data.service';

@Component({
  selector: 'app-appoinments',
  templateUrl: './appoinments.component.html',
  styleUrls: ['./appoinments.component.scss']
})

export class AppoinmentsComponent implements OnInit {
  user_profile;
  callList:any=[];
  callListattendee:any=[];
  constructor(private _fd:FetchDataService) { }

  ngOnInit(): void {
    this.user_profile = JSON.parse(localStorage.getItem('virtual'));
    this.getScheduleCall();
    this.getScheduleCallatttendee();
  }
  rejectCall(id,attendee_id,name,email,time){
      // let data = JSON.parse(localStorage.getItem('virtual'));
      const formData = new FormData();
//       id:158887
// ec_name:divakar
// email:divakar.kumar@multitvsolution.com
// time:2020-12-01 10:10:10 
      formData.append('self_id', id );
      formData.append('attendee_id', attendee_id );
      formData.append('ec_name', name);
      formData.append('email', email);
      formData.append('time', time);
      this._fd.rejected(formData).subscribe(res=>{
        console.log(res);
        this.getScheduleCallatttendee();
      })
  }
  getScheduleCall(){
    this._fd.getScheduleList(this.user_profile.id).subscribe(res=>{
      console.log(res);
      this.callList = res.result;
    })
  }
  getScheduleCallatttendee(){
    this._fd.getScheduleListatttendee(this.user_profile.id).subscribe(res=>{
      console.log(res);
      this.callListattendee = res.result;
    })
  }
}
