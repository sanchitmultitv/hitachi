import { Component, OnInit, Renderer2 } from '@angular/core';
import {NgbDateStruct, NgbCalendar} from '@ng-bootstrap/ng-bootstrap';
declare var $: any;
@Component({
  selector: 'app-schedule-call',
  templateUrl: './schedule-call.component.html',
  styleUrls: ['./schedule-call.component.scss']
})
export class ScheduleCallComponent implements OnInit {
  model: NgbDateStruct;
  date: {year: number, month: number};
timeVal;
msg;
  constructor(private calendar: NgbCalendar, private renderer: Renderer2) { }

  ngOnInit(): void {
   // Data Picker Initialization

  }
  selectToday() {
    this.model = this.calendar.getToday();
    console.log(this.model);
  }
  closePopup() {
    $('.scheduleCallmodal').modal('hide');
  }
  getTime(event: any, valClass, time) {
// console.log(time);
this.timeVal = time;
const hasClass = event.target.classList.contains(valClass);
$(".time-list li a.active").removeClass("active");
// adding classname 'active' to current click li
this.renderer.addClass(event.target, valClass);
// if (hasClass) {
//   //alert('has')
//       this.renderer.removeClass(event.target, valClass);
//     } else {
//      // alert(valClass)
//       this.renderer.addClass(event.target, valClass);
//     }
  }
  confirm() {
console.log(this.timeVal);
this.msg = 'Time slot successfully successfully saved!';
setTimeout(() => {
  this.msg = '';
}, 2000);
  }
}
