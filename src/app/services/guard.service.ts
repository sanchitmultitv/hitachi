import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class GuardService {

  constructor(public auth: AuthService, public router: Router) { }
  // canActivate(): boolean {
  //   if (localStorage.getItem('virtual')) {
  //   this.router.navigateByUrl('/lobby');
  //     return false;
  //   }
  //    return true;
  // }
}
